# TAps

Una aplicación creada por miembros de la comunidad con el fin de mejorar nuestra estadía en los sistemas GNU/Linux, fácil de usar y pensada para todos y cada uno de los usuarios.

## Concepto

Analizador de Tiempo de Uso de Aplicaciones en cualquier sistema GNU/Linux

## Objetivo

Permitir la visualización del tiempo de uso de cada aplicación abierta en un periodo de tiempo especifico. 

## Funciones

1. Mostrar, de manera acumulativa, las horas y minutos consumidos
2. Brindar estadísticas diarias, semanales, mensuales y anuales 
3. Establecer e informar de un período determinado de tiempo de uso de cada aplicación (v. 1x)
4. Bloquear aplicaciones durante un tiempo establecido (v. 2x)

## Públicos Objetivos

Personas quienes pretendan controlar y reducir (u optimizar) su tiempo frente a la pantalla de sus computadoras

## Visión

Convertirse en la aplicación referente en GNU/Linux

## Filosofía

1. Solidaridad
2. Cooperación bilateral
3. Seguir Las Cuatro Libertades del Software Libre
4. Gratuidad
5. Multientorno de escritorio